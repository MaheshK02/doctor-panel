<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;

class UserController extends Controller
{
    public function actionUserInfo()
    {
        
        return $this->render('user-info');
    }
    public function actionUser()
    {
        
        return $this->render('user');
    }
}
