<?php

/* @var $this yii\web\View */

$this->title = 'Doctor Verification';
?>
<div class="container">
    <h2 style="text-align: center;">Doctor Verification</h2>
    <!-- <p>The form below contains two dropdown menus (select lists):</p> -->
    <br/>
    <div class="row">
    <form>
        <div class="form-group col-md-2" >
                  <select class="form-control" id="sel1">
                    <option>All</option>
                    <option>Approved</option>
                    <option>Disapproved</option>
                    <!-- <option>4</option> -->
                  </select>
            </div>
            <div class="col-md-3">
                    <input class="form-control"  type="search" id="search" value="" placeholder="Search name here...">
            </div>
            <div class="col-md-3" id="sandbox-container">
                        <!-- <div class='input-group date' id='datetimepicker2'>
                            <input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div> -->
                        <div class="input-group date">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-calendar"></i>
                            </span>
                            <input type="text" class="form-control" placeholder="YYYY/MM/DD">
                        </div>
            </div>
            <div class="col-md-3" id="sandbox-container">
                         <div class="input-group date">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-calendar"></i>
                            </span>
                            <input type="text" class="form-control" placeholder="YYYY/MM/DD">
                        </div>
            </div>
            <div class="col-md-1 clearfix" >
                    <button type="button" class="btn btn-primary">Submit</button>
            </div>
    </form>
    </div>
</div>
<div class="container" style="padding-top: 15px;">
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Name</th>
        <th>Mobile</th>
        <th>Email</th>
        <th>Location</th>
        <th>MCI Number</th>
        <th>Action</th>
        <th style="text-align: center;">Operation</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>John Kadam</td>
        <td>9820151515</td>
        <td>john@example.com</td>
        <td>Mumbai</td>
        <td>abcd1212</td>
        <td >
            <button type="button" class="btn btn-link"><b>Edit</b></button>
        </td>
        <td style="text-align: center;">
            <button type="button" class="btn btn-success">Approve</button>
            <button type="button" class="btn btn-danger">Reject</button>
        </td>
      </tr>
      <tr>
        <td>John Kadam</td>
        <td>9820151515</td>
        <td>john@example.com</td>
        <td>Mumbai</td>
        <td>abcd1212</td>
        <td>
            <button type="button" class="btn btn-link"><b>Edit</b></button>
        </td>
        <td style="text-align: center;">
            <button type="button" class="btn btn-success">Approve</button>
            <button type="button" class="btn btn-danger">Reject</button>
        </td>
      </tr>
    </tbody>
  </table>
</div>
