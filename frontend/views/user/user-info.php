<?php

/* @var $this yii\web\View */
$this->title = 'Claim Profile'; 
?>

<div class="container-fluid" ng-app="app-user-info" ng-controller="controller-user-info">
    <!-- <h2 style="text-align: center;">The Ayurveda Network</h2> -->
    <div ng-hide="searchdata">
        <h3 style="text-align: center;">Dear Doctor Welcome to Curofy! </h3>
        <h4 style="text-align: center;"> Enter Your First Name & Last Name to Claim Your Profile </h4>
        <br/>
        <div class="row">
        <form>
                <div class="form-group col-md-5"  >
                        <input class="form-control" ng-model="fname" type="search" id="" value="" placeholder="Enter First Name " required style="height: 48px;">
                </div>
                <div class="form-group col-md-5 clearfix">
                        <input class="form-control" ng-model="lname" type="search" id="" value="" placeholder="Enter Last Name" 
                        required style="height: 48px;">
                </div>
              <!--   <div class="form-group col-md-4 clearfix" >
                      <select class="form-control" ng-model="singleState" ng-options="o.name for o in singleStates"></select>
                </div> -->
                <div class="col-md-2 clearfix"  >
                        <button type="button" ng-click="search()" class="btn btn-block btn-lg btn-success">Search</button>
                </div>
        </form>
        </div>
        <br>
    </div>


    <div class="container" ng-repeat="user in users" ng-hide="showdata" >
        <div class="row" >
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="well well-sm" style="background-color: #fff;">
                    <div class="row">
                        <!-- <div class="col-sm-6 col-md-4">
                            <img src='{{user.imageUrl}}'  alt="" class="img-rounded img-responsive" />id="" style="color: #6A9B29;"
                        </div> -->
                        <div class="col-sm-12" style="font-size: 16px">
                            <div class="row"> 
                                <div class="col-sm-12">
                                    <div class="col-md-3">
                                        <p>
                                        <span> Name:</span> 
                                        </p>
                                    </div> 
                                    <div class="col-md-6">
                                        <p>
                                        <span style="font-weight: 600;font-size: 18px;"> {{user.name}}</span>
                                        <b ng-if="!user.name">NA</b>
                                        </p>
                                    </div>                               
                                </div>
                            </div>
                            <div class="row"> 
                            <div class="col-sm-12">
                                <div class="col-md-3">
                                    <p> <span> Qualification:</span> </p>
                                </div> 
                                <div class="col-md-6">
                                   <p> <span style="font-weight: 600;font-size: 18px;"> {{user.qualification}}</span>
                                    <b ng-if="!user.qualification">NA</b> </p>
                                </div>                               
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-sm-12">
                                <div class="col-md-3">
                                   <p> <span style="font-size: 14px;" >Address:</span></p>
                                </div> 
                                <div class="col-md-6">
                                    <p> <span style="font-weight: 600;font-size: 16px;"> {{user.address}}</span>
                                     <b ng-if="!user.address">NA</b> </p>
                                </div>                               
                            </div>
                         </div>
                         <div class="row"> 
                            <div class="col-sm-12">
                                <div class="col-md-3">
                                   <p> <span style="font-size: 14px;" >University Name:</span></p>
                                </div> 
                                <div class="col-md-6">
                                    <p> <span style="font-weight: 600;font-size: 18px;"> {{user.universityName}}</span>
                                     <b ng-if="!user.universityName">NA</b> </p>
                                </div>                               
                            </div>
                         </div>   
                         <!-- <div class="row"> 
                            <div class="col-sm-12">
                                <div class="col-md-3">
                                   <p> <span style="font-size: 14px;" >Registration Number:</span></p>
                                </div> 
                                <div class="col-md-6">
                                    <p> <span style="font-weight: 600;font-size: 18px;"> {{user.registrationNumber}}</span>
                                     <b ng-if="!user.registrationNumber">NA</b> </p>
                                </div>                               
                            </div>
                         </div> -->   
                         <div class="row"> 
                            <div class="col-sm-12">
                                <div class="col-md-3">
                                   <p> <span style="font-size: 14px;" >State Medical Council:</span></p>
                                </div> 
                                <div class="col-md-6">
                                    <p> <span style="font-weight: 600;font-size: 18px;"> {{user.stateMedicalCouncil}}</span>
                                     <b ng-if="!user.stateMedicalCouncil">NA</b> </p>
                                </div>                               
                            </div>
                         </div>    
                         <div class="row">
                             <div class="col-sm-12" style="text-align: center;padding-top: 10px;">
                                  <button type="button" class="btn btn-success" ng-click="show(user.registrationNumber)" style="padding: 10px 50px 10px 50px">Claim</button><br><br>
                             </div>
                         </div>  
                            <!-- Split button -->                             
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4" style="text-align: center;" >

                <!-- <button type="button" class="btn btn-success" ng-click="goLogin()" style="padding: 10px 50px 10px 50px">Yes, this is me</button><br><br><br>

                <button type="button" class="btn" onclick="window.location.reload()" style="padding: 10px 46px 10px 46px">No, this isn't me</button> -->            
            </div>
        </div>
    </div>
   
    <div class="container">
            <div ng-hide="error">
                <h3>No Result Found</h3>
            </div>
        
    </div> 
    <div class="container">
         
        <div class="col-md-12 well well-sm"  style="background-color: #fff;" style="text-align: center;" ng-hide="showmci" >
             <div class="row">
                <div style="text-align: center;">
                     <p>
                        We Only Allow Registered Medical Practitioners on Curofy <br>
                        Enter Your MCI Number to Verify Your Identity                   
                    </p><br>
                </div>
                <div class="col-md-4 col-md-offset-4" style="text-align: center;">
                    <form>
                        <div class="form-group" >
                                <input class="form-control" ng-model="mci" type="search" id="" value="" placeholder="Enter MCI Number" required style="height: 48px;">
                        </div>
                        <div ng-hide="errormci" >
                            <p style="color: red;">Please enter valid mci number</p>
                        </div> 
                        <div class="clearfix"  >
                                <button type="button" ng-click="checkmci()" class="btn btn-block btn-lg btn-success">Verify</button>
                        </div><br>
                        <div>
                            <a href="" ng-click="showlist()">Go Back to the list</a>
                        </div>
                        <br>
                    </form>
                </div>
            
            </div>
        </div>

        
    </div>  
    
</div>

