var appDoctorsInfo = angular.module('app-user-info', []);

appDoctorsInfo.controller('controller-user-info', function($rootScope, $scope, $http) {

	// $scope.singleSelects = {
	// 		{0: 'Rejected'}, {1: 'Approved'}, {2: 'All'}, {3: 'Incomplete Registration'}
	// };
	// $scope.singleStates = [{ name: "Andhra Pradesh Medical Council", id: 1 },
	// 					   { name: "Assam Medical Council", id: 2 },
	// 					   { name: "Bihar Medical Council", id: 3 },
	// 					   { name: "Bombay Medical Council", id: 4 },
	// 					   { name: "Chattisgarh Medical Council", id: 5 },
	// 					   { name: "Delhi Medical Council", id: 6 },
	// 					   { name: "Goa Medical Council", id: 7 },
	// 					   { name: "Gujarat Medical Council", id: 8 },
	// 					   { name: "Haryana Dental  Medical Council", id: 9 },
	// 					   { name: "Hyderabad Medical Council", id: 10 },
	// 					   { name: "Jammu Kashmir Medical Council", id: 11 },
	// 					   { name: "Jharkhand Medical Council", id: 12 },
	// 					   { name: "Karnataka Medical Council", id: 13 },
	// 					   { name: "Madhya Pradesh Medical Council", id: 14 },
	// 					   { name: "Madras Medical Council", id: 15 },
	// 					   { name: "Maharashtra Medical Council", id: 16 },
	// 					   { name: "Medical Council of India", id: 17 },
	// 					   { name: "Mysore Medical Council", id: 18 },
	// 					   { name: "Orissa Council of Medical Registration", id: 19 },
	// 					   { name: "Punjab Medical Council", id: 20 },
	// 					   { name: "Rajasthan Medical Council", id: 21 },
	// 					   { name: "Tamil Nadu Medical Council", id: 22 },
	// 					   { name: "Trivancore Medical Council, Cochin", id: 23 },
	// 					   { name: "Uttar Pradesh Medical Council", id: 24 },
	// 					   { name: "Uttarakhand Medical Council", id: 25 },
	// 					   { name: "Vidharba Medical Council", id: 26 },
	// 					   { name: "West Bengal Medical Council", id: 27 }];
	// $scope.singleState = $scope.singleStates[0];

	$scope.users = [];
	$scope.error = true;
	$scope.showmci = true;
	// $scope.errormci = true;

	$scope.show = function(registrationNumber) {
		$scope.showdata   = true;
		$scope.showmci    = false;
		$scope.errormci   = true;
		$scope.searchdata = true;
		registrationNumber: registrationNumber

		console.log(registrationNumber);

		$scope.checkmci = function($location){
				
			$scope.errormci = true;
				 
			if (registrationNumber == $scope.mci) {
				 window.location = "https://curofy.com";
			}
			else{
				$scope.errormci = false;
			}
			console.log($scope.mci);
		}

	}

	$scope.showlist = function(){
		$scope.showdata = false;
		$scope.searchdata = false;
		$scope.showmci = true;
	}

	$scope.search = function() {
		

		var request = {
			method: 'POST',
			url: 'http://52.41.150.171/Ayushsocial2/demo/check/mci',
			 headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			 transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
			data: {
				firstName: $scope.fname,
				lastName: $scope.lname,  //'2017-06-07',  
				
			}
		};

		$http(request)
			.then(function(response) {
				if (response.data.length > 0 ){
						$scope.users = response.data;
						$scope.error = true;
						$scope.showmci = true;
						$scope.showdata = false;
				}else
				{
					$scope.users = [];
					$scope.error = false;
				}
				console.log(response.data.length);
			}, function(error) {
					$scope.users = [];
					$scope.error = false;
			});
	};

	$scope.goLogin = function($scope, $location) {
        window.location = "https://curofy.com";
    };






	// $scope.approveOrReject = function(userId, status, index) {
 //        var request = {
 //            method: 'POST',
 //            url: 'http://52.41.150.171/analyticsapi/analytics/doctor/approve-or-reject',
 //            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
 //            transformRequest: function (obj) {
 //                var str = [];
 //                for (var p in obj)
 //                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
 //                return str.join("&");
 //            },
 //            data: {
 //              userId: userId,
 //              status: status
 //            }
 //        };
 //        $http(request)
 //            .then(
 //            function (response) {
 //              //if (response.data.status == 1) {
 //              		$scope.users.splice(index, 1);
 //              //}
 //            },
 //            function (error) {
 //              console.log(error);
 //            }
 //        );
 //    };

	// $scope.info = function(userId) {

	// 	console.log(userId);
	// 	var request = {
	// 		method: '',
	// 		url: '',
	// 		params: {
	// 			userId: userId,
	// 			status: 0
	// 		}
	// 	};

	// 	$http(request)
	// 		.then(function(response) {
	// 			alert("Done");
				
	// 		}, function(error) {
	// 			alert("Fail");

	// 		});
	// };

});