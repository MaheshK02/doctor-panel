
<div class="container-fluid" style="padding-top: 20px;">
  <h1 class="page-header">Edit Profile</h1>
    <div class="row">
        <!-- left column -->
        <div class="col-md-3">
            <div class="text-center">
                <img src="http://lorempixel.com/200/200/people/9/" class="avatar img-circle img-thumbnail" alt="avatar">
                <h6>Upload a different photo...</h6>
                <input type="file" class="text-center center-block well well-sm">
            </div>
        </div>
    <!-- edit form column -->
        <div class="col-md-9">
            <div class="alert alert-info alert-dismissable">
                <a class="panel-close close" data-dismiss="alert">×</a> 
                <i class="fa fa-coffee"></i>
                This is an <strong>.alert</strong>. Use this to show important messages to the user.
            </div>
            <form class="form-horizontal">
            <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="textinput">Name</label>  
                          <div class="col-md-6">
                          <input id="textinput" name="textinput" type="text" placeholder="Enter name here" class="form-control input-md">
                          </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="textinput">Email</label>  
                          <div class="col-md-6">
                          <input id="textinput" name="textinput" type="text" placeholder="Enter email here" class="form-control input-md">
                            
                          </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="textinput">Mobile</label>  
                          <div class="col-md-6">
                          <input id="textinput" name="textinput" type="text" placeholder="Enter Mobile Number" class="form-control input-md">
                            
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="selectbasic">Gender</label>
                          <div class="col-md-4">
                            <select id="selectbasic" name="selectbasic" class="form-control">
                              <option value="1">Male</option>
                              <option value="2">Female</option>
                              <option value="2">Other</option>
                            </select>
                          </div>
                        </div>

                        <!-- Select Basic -->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="selectbasic">Location</label>
                          <div class="col-md-4">
                            <select id="selectbasic" name="selectbasic" class="form-control">
                              <option value="1">Maharashtra</option>
                              <option value="2">Goa</option>
                            </select>
                          </div>
                        </div>

                        <!-- Textarea -->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="textarea">Summary</label>
                          <div class="col-md-6">                     
                            <textarea class="form-control" id="textarea" name="textarea">trade show introduction</textarea>
                          </div>
                        </div>

                        <!-- File Button --> 
                        <!-- <div class="form-group">
                          <label class="col-md-4 control-label" for="filebutton">upload photo</label>
                          <div class="col-md-4">
                            <input id="filebutton" name="filebutton" class="input-file" type="file">
                          </div>
                        </div>  -->   
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Mci Number</label>  
                            <div class="col-md-6">
                            <input id="textinput" name="textinput" type="text" placeholder="Enter Mci Number" class="form-control input-md">
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="textinput">Speciality</label>  
                          <div class="col-md-6">
                          <input id="textinput" name="textinput" type="text" placeholder="Enter Speciality" class="form-control input-md">
                          </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="selectbasic">User Type</label>
                          <div class="col-md-4">
                            <select id="selectbasic" name="selectbasic" class="form-control">
                              <option value="1">Default</option>
                              <option value="2">Doctor</option>
                              <option value="2">Student</option>
                            </select>
                          </div>
                        </div>

                        <!-- Select Basic -->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="selectbasic">Status</label>
                          <div class="col-md-4">
                            <select id="selectbasic" name="selectbasic" class="form-control">
                              <option value="1">Default</option>
                              <option value="2">Approved</option>
                              <option value="2">Reject</option>
                            </select>
                          </div>
                        </div>
                        
                    </div>
                </div>       
                <div class="row" style="text-align: center;">
                    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Submit</button>
                </div> 
            </form>  
            </div> 
    </div>
</div>
