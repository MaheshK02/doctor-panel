<?php

/* @var $this yii\web\View */
$this->title = 'Doctor Verification';
?>

<div class="container-fluid" ng-app="app-doctors-info" ng-controller="controller-doctors-info">
    <h2 style="text-align: center;">Doctor Verification</h2>
    <br/>
    <div class="row">
    <form>
        <div class="form-group col-md-2" >
                  <select class="form-control" ng-model="singleSelect" ng-options="o.name for o in singleSelects"></select>
            </div>
            <div class="col-md-3">
                    <input class="form-control" ng-model="searchText" type="search" id="search" value="" placeholder="Search name here...">
            </div>
            <div class="col-md-3" id="sandbox-container">
                        <!-- <div class='input-group date' id='datetimepicker2'>
                            <input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div> -->
                        <div class="input-group date">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-calendar"></i>
                            </span>
                            <input type="date" ng-model="startDate" class="form-control" placeholder="YYYY/MM/DD" required>
                        </div>
            </div>
            <div class="col-md-3" id="sandbox-container">
                         <div class="input-group date">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-calendar"></i>
                            </span>
                            <input type="date" ng-model="endDate" class="form-control" 
                            placeholder="YYYY/MM/DD" required>
                        </div>
            </div>
            <div class="col-md-1 clearfix" >
                    <button type="button" ng-click="search()" class="btn btn-primary">Submit</button>
            </div>
    </form>
    </div>
    <br>

  <!--   <table>
        <tr>
            <th>City Name</th>
            <th>Image</th>
            <th>Action</th>
        </tr>
        <tr ng-repeat="city in cities">
            <td>{{city.name}}</td>
            <td><img ng-src="{{city.imageUrl}}" width="100px" /></td>
            <td><button type="button" class="btn btn-danger" ng-click="logCity(city.id)">SHOW ID</button></td> 
        </tr>
    </table> -->

  <table class="table table-hover">
    <thead>
      <tr>
        <th style="text-align: center;">Id</th>
        <th style="text-align: center;">Name</th>
        <th style="text-align: center;">Mobile</th>
        <th style="text-align: center;">Email</th>
        <th style="text-align: center;">MCI Number</th>
        <th style="text-align: center;">Location</th>
        <th style="text-align: center;">Action</th>
        <th style="text-align: center;">Operation</th>
      </tr>
    </thead>
    <tbody>
      <tr ng-hide="loading" ng-repeat="user in users | filter:searchText">
        <td style="text-align: center;vertical-align: middle;padding: 6px;">
            {{user.id}}
        </td>
        <td style="text-align: center;vertical-align: middle;padding: 6px;">
            {{user.name}}<b ng-if="!user.name">NA</b>
        </td> 
        <td style="text-align: center;vertical-align: middle;padding: 6px;">
            {{user.mobileNumber}}<b ng-if="!user.mobileNumber">NA</b>
        </td>
        <td style="text-align: center;vertical-align: middle;padding: 6px;">
            {{user.email}}<b ng-if="!user.email">NA</b>
        </td>
        <td style="text-align: center;vertical-align: middle;padding: 6px;">
            {{user.mciNumber}}<b ng-if="!user.mciNumber">NA</b>
        </td>
        <td style="text-align: center;vertical-align: middle;padding: 6px;">
            {{user.location}}<b ng-if="!user.location">NA</b>
        </td>
        <td style="text-align: center;vertical-align: middle;padding: 6px;">
            <button type="button" class="btn btn-link"><b>Edit</b></button>
        </td>
        <td style="text-align: center;">
            <button type="button" class="btn btn-success" ng-click="approveOrReject(user.id, 1, $index)">Approve</button>
            <button type="button" class="btn btn-danger" ng-click="approveOrReject(user.id, 0, $index)">Reject</button>
        </td>
      </tr>
    </tbody>
  </table>

</div>

