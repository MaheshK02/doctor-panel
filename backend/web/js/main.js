var appDoctorsInfo = angular.module('app-doctors-info', []);

appDoctorsInfo.controller('controller-doctors-info', function($rootScope, $scope, $http) {

	// $scope.singleSelects = {
	// 		{0: 'Rejected'}, {1: 'Approved'}, {2: 'All'}, {3: 'Incomplete Registration'}
	// };
	$scope.singleSelects = [{ name: "All", id: 2 },{ name: "Rejected", id: 0 },{ name: "Approved", id: 1 }, { name: "Incomplete Registration", id: 3 }];
	$scope.singleSelect = $scope.singleSelects[0];

	$scope.users = [];
	$scope.search = function() {

		var request = {
			method: 'GET',
			url: 'http://52.41.150.171/analyticsapi/analytics/user/web/get',
			params: {
				name: $scope.name,
				startDate: $scope.startDate,  //'2017-06-07', 
				endDate:  $scope.endDate, //'2017-08-07', 
				userType: $scope.singleSelect.id,
			}
		};

		$http(request)
			.then(function(response) {
				$scope.users = response.data;
				//console.log(response.data);
			}, function(error) {

			});
	};

	$scope.approveOrReject = function(userId, status, index) {
        var request = {
            method: 'POST',
            url: 'http://52.41.150.171/analyticsapi/analytics/doctor/approve-or-reject',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: {
              userId: userId,
              status: status
            }
        };
        $http(request)
            .then(
            function (response) {
              //if (response.data.status == 1) {
              		$scope.users.splice(index, 1);
              //}
            },
            function (error) {
              console.log(error);
            }
        );
    };

	$scope.info = function(userId) {

		console.log(userId);
		var request = {
			method: '',
			url: '',
			params: {
				userId: userId,
				status: 0
			}
		};

		$http(request)
			.then(function(response) {
				alert("Done");
				
			}, function(error) {
				alert("Fail");

			});
	};

});