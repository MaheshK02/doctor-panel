<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;

class DataController extends Controller
{
    public function actionDoctorsInfo()
    {
        
        return $this->render('doctors-info');
    }
    public function actionUser()
    {
        
        return $this->render('user');
    }
}
